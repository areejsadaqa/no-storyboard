//
//  SignUPViewController.swift
//  No StoryBoard
//
//  Created by areej on 10/9/20.
//  Copyright © 2020 areej. All rights reserved.
//

import UIKit

class SignUPViewController: UIViewController {
    let emailTextField: UITextField = {
        let e = UITextField()
        // e.placeholder = "Email"
        //e.textColor = .white
        
        let attPlaceHolder = NSAttributedString(string: "Email", attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        e.attributedPlaceholder = attPlaceHolder
        e.backgroundColor = violetTheme
        return e
    }()
    let firstNameTextField: UITextField = {
        let e = UITextField()
        // e.placeholder = "Email"
        //e.textColor = .white
        
        let attPlaceHolder = NSAttributedString(string: "Fist Name", attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        e.attributedPlaceholder = attPlaceHolder
        e.backgroundColor = violetTheme
        return e
    }()
    let lastNameTextField: UITextField = {
        let e = UITextField()
        // e.placeholder = "Email"
        //e.textColor = .white
        
        let attPlaceHolder = NSAttributedString(string: "Last Name", attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        e.attributedPlaceholder = attPlaceHolder
        e.backgroundColor = violetTheme
        return e
    }()
    
    let passwordTextField: UITextField = {
        let p = UITextField()
        // p.placeholder = "Password"
        // p.textColor = .white
        let attPlaceHolder = NSAttributedString(string: "Password", attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        p.attributedPlaceholder = attPlaceHolder
        p.backgroundColor = violetTheme
        return p
    }()
    let confirmPasswordTextField: UITextField = {
        let p = UITextField()
        // p.placeholder = "Password"
        // p.textColor = .white
        let attPlaceHolder = NSAttributedString(string: "Confirm Password", attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        p.attributedPlaceholder = attPlaceHolder
        p.backgroundColor = violetTheme
        return p
    }()
    let signUpButton: UIButton = {
        let l = UIButton(type: .system)
        l.setTitleColor(.white , for: .normal)
        l.setTitle("Log In", for: .normal)
        l.backgroundColor = .purple
        l.layer.cornerRadius = 5
        //l.addTarget(self, action: #selector(openLogInView), for: .touchUpInside)
        return l
    }()
  
    

    override func viewDidLoad() {
        super.viewDidLoad()

      view.backgroundColor = violetTheme
        setupEmailTextField()
        setupFirstNameTextField()
        setupLastNameTextField()
        setupPasswordTextField()
        setupconfirmPasswordTextField()
    }
    
    fileprivate func  setupEmailTextField() {
        view.addSubview(emailTextField)
        emailTextField.anchors(top: view.safeAreaLayoutGuide.topAnchor, topPad: 250, bottom: nil, bottomPad: 0, left: view.safeAreaLayoutGuide.leftAnchor, leftPad: 24, right: view.safeAreaLayoutGuide.rightAnchor, rightPad: 24, height: 50, width: 0)
       
        
    }
    fileprivate func  setupFirstNameTextField() {
        view.addSubview(firstNameTextField)
        firstNameTextField.anchors(top: emailTextField.bottomAnchor, topPad: 8, bottom: nil, bottomPad: 0, left: emailTextField.leftAnchor, leftPad: 0, right: emailTextField.rightAnchor, rightPad: 0, height: 50, width: 0)
       
        
    }
    fileprivate func  setupLastNameTextField() {
        view.addSubview(lastNameTextField)
        lastNameTextField.anchors(top: firstNameTextField.bottomAnchor, topPad: 8, bottom: nil, bottomPad: 0, left: firstNameTextField.leftAnchor, leftPad: 0, right: firstNameTextField.rightAnchor, rightPad: 0, height:50, width: 0)
        
        
    }
    fileprivate func  setupPasswordTextField() {
        view.addSubview(passwordTextField)
        passwordTextField.anchors(top: lastNameTextField.bottomAnchor, topPad: 8, bottom: nil, bottomPad: 0, left: lastNameTextField.leftAnchor, leftPad: 0, right: lastNameTextField.rightAnchor, rightPad: 0, height: 50, width: 0)
        
        
    }


    fileprivate func  setupconfirmPasswordTextField() {
        view.addSubview(confirmPasswordTextField)
        confirmPasswordTextField.anchors(top: passwordTextField.bottomAnchor, topPad: 8, bottom: nil, bottomPad: 0, left: passwordTextField.leftAnchor, leftPad: 0, right: passwordTextField.rightAnchor, rightPad: 0, height: 50, width: 0)
        
        
    }
}
