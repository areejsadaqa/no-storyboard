//
//  ViewController.swift
//  No StoryBoard
//
//  Created by areej on 10/9/20.
//  Copyright © 2020 areej. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    let emailTextField: UITextField = {
        let e = UITextField()
       // e.placeholder = "Email"
        //e.textColor = .white
        
        let attPlaceHolder = NSAttributedString(string: "Email", attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        e.attributedPlaceholder = attPlaceHolder
        e.backgroundColor = violetTheme
        return e
    }()
    let passwordTextField: UITextField = {
        let p = UITextField()
       // p.placeholder = "Password"
        // p.textColor = .white
        let attPlaceHolder = NSAttributedString(string: "Password", attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        p.attributedPlaceholder = attPlaceHolder
        p.backgroundColor = violetTheme
        return p
    }()
    
    let logInButton: UIButton = {
       let l = UIButton(type: .system)
        l.setTitleColor(.white , for: .normal)
        l.setTitle("Log In", for: .normal)
        l.backgroundColor = .purple
        l.layer.cornerRadius = 5
        l.addTarget(self, action: #selector(openLogInView), for: .touchUpInside)
        return l
    }()
    let forgotPasswordButton: UIButton = {
        let f = UIButton(type: .system)
        f.setTitleColor(.white , for: .normal)
        f.setTitle("Forgot Password?", for: .normal)
        f.backgroundColor = violetTheme
        return f
    }()
    
    let haveAccountButton: UIButton = {
        let color = UIColor(red: 148 / 255, green: 0 / 255, blue: 211 / 255, alpha: 1)
        let font = UIFont.systemFont(ofSize: 16)
        
       let h = UIButton(type: .system)
        h.backgroundColor = violetTheme
        let attributedTitle = NSMutableAttributedString(string: "Don't have an account? " , attributes: [NSAttributedString.Key.foregroundColor: color , NSAttributedString.Key.font: font ]) //allow u to append att str
        attributedTitle.append(NSMutableAttributedString(string: "Sign Up", attributes: [NSAttributedString.Key.foregroundColor: UIColor.white,NSAttributedString.Key.font: font]))
        
        h.setAttributedTitle(attributedTitle, for: .normal)
        h.addTarget(self, action: #selector(singUpView), for: .touchUpInside)
        return h
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        view.backgroundColor = violetTheme
        setupTexFieldComponents()
        setupLogInButton()
        setupHaveAccountButton()
        setupForgotPasswordButton()
         openLogInView()
         singUpView()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle{
        return .lightContent // color of statusbar to white
    }
    
    fileprivate func setupTexFieldComponents() {
        // no other classes outside viewController have access to it
        
        setupEmailField()
        setupPasswordField()
    }
    fileprivate func setupEmailField(){
        view.addSubview(emailTextField)
        emailTextField.anchors(top: nil, topPad: 0, bottom: nil, bottomPad: 0, left: view.leftAnchor, leftPad: 24, right: view.rightAnchor, rightPad: 24, height: 30, width: 0)
        
        ///emailTextField.translatesAutoresizingMaskIntoConstraints = false
        //emailTextField.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 16).isActive = true
       /// emailTextField.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 24).isActive = true
       /// emailTextField.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -24).isActive = true
        //left and right anchor can know alreadly what width it is but the height not
        
      ///  emailTextField.heightAnchor.constraint(equalToConstant: 30).isActive = true
        emailTextField.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        // justY center cause its refer to top and button , we dont need X cause it's left and right
    }
    fileprivate func setupPasswordField(){
        view.addSubview(passwordTextField)
        passwordTextField.anchors(top: emailTextField.bottomAnchor, topPad: 8, bottom: nil, bottomPad: 0, left: emailTextField.leftAnchor, leftPad: 0, right: emailTextField.rightAnchor, rightPad: 0, height: 30, width: 0)
        
        
       /// passwordTextField.rightAnchor.constraint(equalTo: emailTextField.rightAnchor, constant: 0).isActive = true // constant '0' cause emailTextField alreadlly have 24
      
    }
    
    fileprivate func  setupLogInButton() {
        view.addSubview(logInButton)
        logInButton.anchors(top: passwordTextField.bottomAnchor, topPad: 8, bottom: nil, bottomPad: 0, left: passwordTextField.leftAnchor, leftPad: 0, right: passwordTextField.rightAnchor, rightPad: 0, height: 50, width: 0)
        
      
    }
    
    fileprivate func setupForgotPasswordButton() {
        view.addSubview(forgotPasswordButton)
        forgotPasswordButton.anchors(top: logInButton.bottomAnchor, topPad: 8, bottom: nil, bottomPad: 0, left: logInButton.leftAnchor, leftPad: 0, right: logInButton.rightAnchor, rightPad: 0, height: 30, width: 0)
      
        
    }
    
    fileprivate func  setupHaveAccountButton() {
        view.addSubview(haveAccountButton)
        haveAccountButton.anchors(top: nil, topPad: 0, bottom: view.safeAreaLayoutGuide.bottomAnchor, bottomPad: 8, left: view.leftAnchor, leftPad: 12, right: view.rightAnchor, rightPad: 12, height: 30, width: 0)
        
    }
    
    
    @objc func  openLogInView(){
        let openView = OpenLogInViewController()
        openView.navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Dismiss", style: .plain, target: self, action: #selector(dismissSelf))
        let navVC = UINavigationController(rootViewController: openView)
        navVC.modalPresentationStyle = .custom
        present(navVC, animated: true, completion: nil)
    }
    
    @objc func singUpView() {
        let signUp = SignUPViewController()
       signUp.navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Dismiss", style: .plain, target: self, action: #selector(dismissSelf))
        let navVC = UINavigationController(rootViewController: signUp)
        navVC.modalPresentationStyle = .custom
        present(navVC, animated: true, completion: nil)
    }
    
    @objc func dismissSelf(){
        dismiss(animated: true, completion:nil)
    }
}

