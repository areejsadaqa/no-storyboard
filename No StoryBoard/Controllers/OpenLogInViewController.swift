//
//  OpenLogInViewController.swift
//  No StoryBoard
//
//  Created by areej on 10/9/20.
//  Copyright © 2020 areej. All rights reserved.
//

import UIKit

class OpenLogInViewController: UIViewController {
    
    let label: UILabel = {
        let l = UILabel()
        l.textColor = .white
        l.textAlignment = .center
        l.backgroundColor = violetTheme
        l.text = "WELCOME"
        return l
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = violetTheme
        title = "Hello"
        setupLabel()

       
    }
    

    func setupLabel() {
        view.addSubview(label)
        label.anchors(top: nil, topPad: 0, bottom: nil, bottomPad: 0, left: view.safeAreaLayoutGuide.leftAnchor, leftPad: 8, right: view.safeAreaLayoutGuide.rightAnchor, rightPad: 8, height: 30, width: 0)
        label.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true 
    }

}
